﻿using UnityEngine;

public class TopMainMenu : MonoBehaviour
{
    public GameObject topMainMenuPanel;
    public GameObject selectGameMenuPanel;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TopMainMenu1();
        }
    }

    public void SelectGameMenu()
    {
        topMainMenuPanel.SetActive(false);
        selectGameMenuPanel.SetActive(true);
    }

    public void TopMainMenu1()
    {
        topMainMenuPanel.SetActive(true);
        selectGameMenuPanel.SetActive(false);
    }

    public void SelectLevel(string levelName)
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(levelName);
    }

    public void ExitGame()
    {
        Application.Quit();
    }
}
