﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControl : MonoBehaviour
{
    public void Play()
    {
        SceneManager.LoadScene("BunnyGame");
    }

    public void MainMenu()
    {
        SceneManager.LoadScene("TopMainMenu");
    }

    public void Exit()
    {
        Debug.Log("Application Quit");
        Application.Quit();
    }
}
