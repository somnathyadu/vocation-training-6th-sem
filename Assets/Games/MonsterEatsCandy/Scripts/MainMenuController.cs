﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MonsterEatsCandy
{
    public class MainMenuController : MonoBehaviour
    {
        public void Play()
        {
            SceneManager.LoadScene("MonsterGame");
        }
        public void MainMenu()
        {
            SceneManager.LoadScene("TopMainMenu");
        }

        public void Exit()
        {
            Application.Quit();
        }

    }
}