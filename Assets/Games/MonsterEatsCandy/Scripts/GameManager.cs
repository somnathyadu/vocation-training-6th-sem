﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace MonsterEatsCandy
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;

        public GameObject livesHolder;
        public GameObject gameOverPanel;
        public GameObject pauseMenuPanel;

        int score = 0;
        int lives = 3;
        public Text scoreText;
        bool gameOver = false;

        private void Awake()
        {
            instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                PauseGame();
            }
        }
        public void IncrementScore()
        {
            if (!gameOver)
            {

                score++;
                scoreText.text = score.ToString();

            }


            // print(score);
        }

        public void Decreaselife()
        {
            if (lives > 0)
            {
                lives--;
                print(lives);
                livesHolder.transform.GetChild(lives).gameObject.SetActive(false);

            }
            if (lives <= 0)
            {
                GameOver();
            }
        }

        public void PauseGame()
        {
            pauseMenuPanel.SetActive(true);
            Time.timeScale = 0;
        }

        public void Resume()
        {
            Time.timeScale = 1;
            pauseMenuPanel.SetActive(false);
        }

        public void GameOver()
        {
            CandySpawner.instance.StopSpawningCandies();
            GameObject.Find("Player").GetComponent<PlayerController>().canMove = false;
            gameOverPanel.SetActive(true);

            print("GameOver()");
        }

        public void Restart()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("MonsterGame");
        }

        public void Menu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene("Monstermenu");
        }

    }
}