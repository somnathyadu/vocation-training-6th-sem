﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SatanRun
{
    public class MenuController : MonoBehaviour
    {

        public void Play()
        {
            SceneManager.LoadScene("SantaGame");
        }

        public void MainMenu()
        {
            SceneManager.LoadScene("TopMainMenu");
        }

        public void Exit()
        {
            Application.Quit();
        }
    }
}